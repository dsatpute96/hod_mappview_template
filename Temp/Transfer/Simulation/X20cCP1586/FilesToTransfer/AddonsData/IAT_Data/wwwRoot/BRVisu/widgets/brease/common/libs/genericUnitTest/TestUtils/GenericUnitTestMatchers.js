define([], function () {

    'use strict';

    var GenericUnitTestMatchers = {};

    GenericUnitTestMatchers.compareJQUERYObject =  function (expected) {
        var identicalClass = false,
            identicalID = false,
            identicalTag = false;

        for(var i = 0; i < expected.length; i+=1){    
            if(this.actual[i].attr('class') === expected[i].attr('class')){
                identicalClass = true;
            }
            if(this.actual[i].attr('id') === expected[i].attr('id')){
                identicalID = true;
            }
            if(this.actual[i][0].tagName === expected[i][0].tagName){
                identicalTag = true;
            }
        }

        if(identicalClass && identicalID && identicalTag){
            return true;
        }else{
            return false;
        }
    };

    GenericUnitTestMatchers.checkFunctionBinding = function(expected){
        var identicalEvent = false;

        if(this.actual[0] === expected[0]){
            identicalEvent = true;
        }

        return identicalEvent;
    };

    return GenericUnitTestMatchers;
});