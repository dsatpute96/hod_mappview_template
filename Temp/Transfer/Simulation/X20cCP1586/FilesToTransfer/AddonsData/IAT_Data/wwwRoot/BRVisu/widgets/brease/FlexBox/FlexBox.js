/*global define,brease,console,CustomEvent,_*/
define(['brease/core/ContainerWidget',
    'brease/events/BreaseEvent',
    'brease/decorators/LanguageDependency',
    'brease/enum/Enum',
    'brease/core/Types',
    'brease/helper/Scroller',
    'brease/decorators/DragAndDropCapability'], function (SuperClass, BreaseEvent, languageDependency, Enum, Types, Scroller, dragAndDropCapability) {

    'use strict';

    /**
    * @class widgets.brease.FlexBox
    * #Description
    * widget to group widgets with Label.   
	* @breaseNote 
    * @extends brease.core.ContainerWidget
    * @iatMeta studio:isContainer
    * true
    *
    * @mixins widgets.brease.common.DragDropProperties.libs.DroppablePropertiesEvents
    *
    * @iatMeta category:Category
    * Container
	* @iatMeta description:short
    * Rahmen mit Label
    * @iatMeta description:de
    * Zeigt einen Rahmen um eine Gruppe von Widgets; optional mit einem Beschriftungstext
    * @iatMeta description:en
    * Defines a frame/area where FlexBox item are placed and can expand/shrink as confifured
    */

    /**
    * @cfg {brease.enum.Direction} alignment='vertical'
    * @iatStudioExposed
    * @iatCategory Appearance
    * Alignment of the FlexBoxItems
    * horizontal: elements aligned from left to right.
    * vertical: elements aligned from top to bottom.
    */

    /**
    * @cfg {UInteger} transitionTime=0
    * @iatStudioExposed
    * @iatCategory Behavior
    * Animation time (in ms) for the transition on size change of items - 0 disabled
    */

    /**
    * @property {WidgetList} [children=["widgets.brease.FlexBoxItem"]]
    * @inheritdoc  
    */

    var defaultSettings = {
        alignment: Enum.Direction.vertical,
        transitionTime: 0
    },

    WidgetClass = SuperClass.extend(function FlexBox() {
        SuperClass.apply(this, arguments);
    }, defaultSettings),

    p = WidgetClass.prototype;

    p.init = function () {

        if (this.settings.omitClass !== true) {
            this.addInitialClass('breaseFlexBox');
        }

        SuperClass.prototype.init.call(this);

        _setAlignmentClass(this);

        if (brease.config.editMode) {
            _initEditor(this);
        }

        _applyTransitionTime(this);

    };

    /**
    * @method setAlignment
    * Sets alignment
    * @param {brease.enum.Direction} alignment
    */
    p.setAlignment = function (alignment) {
        this.settings.alignment = alignment;
        _setAlignmentClass(this);
    };

    /**
    * @method getAlignment 
    * Returns alignment.
    * @return {brease.enum.Direction}
    */
    p.getAlignment = function () {
        return this.settings.alignment;
    };

    /**
    * @method setTransitionTime
    * Sets the transition time
    * @param {UInteger} transitionTime
    */
    p.setTransitionTime = function (transitionTime) {
        _applyTransitionTime(this);
        this.settings.transitionTime = transitionTime;
    };

    /**
    * @method getTransitionTime 
    * Returns the transition time
    * @return {UInteger}
    */
    p.getTransitionTime = function () {
        return this.settings.transitionTime;
    };

    //Private

    function _initEditor(widget) {
        require(['widgets/brease/FlexBox/libs/EditorHandles'], function (EditorHandles) {
            var editorHandles = new EditorHandles(widget);
            widget.getHandles = function () {
                return editorHandles.getHandles();
            };
        });
    }

    function _setAlignmentClass(widget) {
        widget.container.removeClass('vertical horizontal');
        if (widget.settings.alignment === Enum.Direction.horizontal) {
            widget.container.addClass('horizontal');
        }
        else {
            widget.container.addClass('vertical');
        }
    }

    function _applyTransitionTime(widget) {
        if (brease.config.editMode) { return; }
        var selector = '#' + widget.elem.id + '> .container';
        $(selector).children().each(function () {
            if ($(this).attr('data-brease-widget') === 'widgets/brease/FlexBoxItem') {
                $(this).css("transition", "flex-grow " + widget.getTransitionTime() / 1000 + "s");
            }
        });
    }

    return dragAndDropCapability.decorate(WidgetClass, false);

});