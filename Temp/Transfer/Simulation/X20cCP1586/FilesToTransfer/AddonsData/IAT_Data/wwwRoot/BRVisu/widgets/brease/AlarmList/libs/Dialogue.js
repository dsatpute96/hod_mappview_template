define([
    'widgets/brease/TableWidget/libs/Dialogue',
    'widgets/brease/AlarmList/libs/FilterSettings',
    'widgets/brease/TableWidget/libs/SortingSettings',
    'widgets/brease/AlarmList/libs/StylingSettings',
    'widgets/brease/AlarmList/libs/DialogueTexts'
], function (SuperClass, Filter, Sorting, Styling, Texts) {
    'use strict';
    /** 
     * @class widgets.brease.AlarmList.libs.Dialogue
     * Class opening and controlling the different configuration dialogues
     */

    var DialogueClass = SuperClass.extend(function Dialogue(widget) {
            SuperClass.apply(this, arguments);
        }, null),

        p = DialogueClass.prototype;

    /**
     * @method initializeFilter
     * @param {String} lang
     * @returns {Object}
     */
    p.initializeFilter = function (lang) {
        this.filter = new Filter(this.dialog, this.widget, lang, this.widget.model.categories);
        this._initializeEmptyDialogConfig(Texts[this.lang].title);
        return this.config;
    };

    /**
     * @method initializeSort
     * @param {String} lang
     * @returns {Object}
     */
    p.initializeSort = function (lang) {
        this.sort = new Sorting(this.dialog, this.widget, lang);               
        this._initializeEmptyDialogConfig(Texts[this.lang].title);
        return this.config;
    };

    /**
     * @method initializeStyle
     * @param {String} lang
     * @returns {Object}
     */
    p.initializeStyle = function (lang) {
        this.style = new Styling(this.dialog, this.widget, lang);               
        this._initializeEmptyDialogConfig(Texts[this.lang].title);
        return this.config;
    };

    return DialogueClass;
});
