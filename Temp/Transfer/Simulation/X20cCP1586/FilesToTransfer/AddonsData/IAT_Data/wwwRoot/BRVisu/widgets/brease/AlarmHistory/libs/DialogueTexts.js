﻿define([], function () {
    'use strict';
    function DialogueTexts() {
        return {
            en: {
                title: 'Configuration dialogue for AlarmHistory',
                style: {
                    title: 'Styling',
                    act: 'Active',
                    inact: 'Inactive',
                    actack: 'Active Acknowledged',
                    inactack: 'Inactive Acknowledged',
                    any: 'Any',
                    sev: 'And severity condition',
                    and: 'and',
                    or: 'or',
                    style: 'Set style',
                    state: 'if new state is'
                }
            },
            de: {
                title: 'Konfigurationsdialog für AlarmHistory',
                style: {
                    title: 'Styling',
                    act: 'Aktiv',
                    inact: 'Inaktiv',
                    actack: 'Aktiv & Bestätigen',
                    inactack: 'Inaktiv & Bestätigen',
                    any: 'Alle',
                    sev: 'und Schweregrad',
                    and: 'und',
                    or: 'oder',
                    style: 'Style festlegen',
                    state: 'wenn Alarm'
                }
            }
        };
    }
    
    return new DialogueTexts();
});
