/*global define*/
define([
    'widgets/brease/TableWidget/libs/Model', 
    'brease/enum/Enum'
], function (SuperClass, Enum) {
    
    'use strict';

    /** 
     * @class widgets.brease.AlarmHistory.libs.Model
     * Class for storing data and manipulating this data
     */

    var defaultSettings = {}, 
    
        ModelClass = SuperClass.extend(function Model(widget) {
            SuperClass.apply(this, arguments);
            this.widget = widget;
            this.categories = [];
            this.firstConnection = true;
        }, defaultSettings), 

        p = ModelClass.prototype;

    /**
     * @method updateTable
     * @private
     * This method will store the retrieved data in the right position, and either append or prepend the data to already existing data, it will also create
     * a second variable to store the original state of the data, this is useful for when the filtering is to take place and there is data that isn't tampered
     * @param {Object[]} data An array of data as defined by the /docs/MpAlarmHistory_WidgetConnection.docx
     * with.
     */
    p.updateTable = function (data) {
        
        if (data === undefined || data.length === 0) { return; }

        if (this.data.length === 0 || data[0].RecordIndex === this.data[0].RecordIndex) {
            this.data = data;
            this.currentData = $.extend(true, [], data); //Store data to calculate 
        } else {
            
            //Extend data accordingly to how it arrives
            if (data[0].RecordIndex < this.data[0].RecordIndex) {
                this.data = this.data.concat(data);
                this.currentData = this.currentData.concat($.extend(true, [], data));
                
            } else if (data[0].RecordIndex > this.data[0].RecordIndex) {
                this.data = data.concat(this.data);
                this.currentData = $.extend(true, [], data).concat(this.currentData);
            }
        }

        //Add Alarm pictures to the alarm list and fix timestamp at the same time
        for (var itr = 0; itr < data.length; itr += 1) {
            data[itr].stn = this._addSVGToRow(data[itr].stn);
            data[itr].sto = this._addSVGToRow(data[itr].sto);
            data[itr].cat = this._fixCategory(data[itr].cat);
            data[itr].tim = this._fixTimestamp(data[itr].tim);
        } 
        
        this.data = _.uniqBy(this.data, 'RecordIndex');
        this.currentData = _.uniqBy(this.currentData, 'RecordIndex');

        if (!brease.config.editMode) {
            this.widget.renderer.updateData();
        }

    };

    /**
     * @method setMockData
     * Only to be called in the editor. Will grab some mock data to display in the editor.
     */
    p.setMockData = function () {
        if (!brease.config.editMode) { return false; }
        this.fetchData(_mockEditorData());
    };

    /**
     * @method getInstanceId
     * This method get's an integer and returns the instance id from the row on that position.
     * @param {Integer} selectedIndex the current poistion of the original data to be returned
     * @returns {String} the instance id of the row given by the selected index
     */
    p.getInstanceId = function (selectedIndex) {
        return this.currentData[selectedIndex].ins;
    };

    /**
     * @method setData
     * intermediary function to handle the telegram in the correct place.
     * @param {Object} telegram
     */
    p.setData = function (telegram) {
        switch (telegram.methodID) {
            case 'GetCategoryList':
                this.setCategories(telegram);
                break;
            default:
                this.fetchData(telegram);
                break;
        }
    };

    /**
     * @method setCategories
     * This method retrieves a telegram with data about the available categories in the backend, it removes duplicates and
     * stores these in a parameter so that when the filter dialog is opened these values will be available.
     * @param {Object} telegram data telegram from backend
     */
    p.setCategories = function (telegram) {
        this.categories = $.extend(true, [], _.uniq(telegram.data));
    };

    /**
     * @method resetTable
     * This method resets the data that has been stored and updates the renderer so that the table becomes empty.
     */
    p.resetTable = function () {
        this.data = [];
        this.currentData = [];
        this.categories = [];
        this.widget.renderer.updateData();
    };

    /**
     * @method reFetchData
     * This method can be called when all stored data should be reset to scratch and new data retrieved from the backend. One such example is at a
     * language change as the old language needs to be removed from the table. It will however not purge the table from data.
     */
    p.reFetchData = function () {
        this.data = [];
        this.currentData = [];
        this.categories = [];
        this.currentIteration = 0;
        if (this.settings.counter > 0) {
            this.sendData(Math.max(this.settings.counter, this.settings.rowsPerFetch), Math.max(this.settings.counter - this.settings.rowsPerFetch, 0));
        }
    };

    /**
     * @method fetchData
     * @private
     * The fetchData is the brain behind the AlarmHistory. It will decided where data should go, if more should be retrieved. When the methodId for the telegram is
     * 'GetUpdateCount' the method will start fetching data if 
     * non is available, interrupt the data fetch if new data has been made available in the backend, continue fetching data where it left of if a page change is
     * made while loading data, or if it should reset the fetch from the very beginning (for example at a language change). If data is returned with the methodId
     * 'GetAlarmHistory' then the data is passed to a function that stores the data and continues the retrieve.
     * @param {Object} telegram
     */
    p.fetchData = function (telegram) {
        if (telegram === null) { return; }
        this.telegram = $.extend(true, [], telegram);

        if (this.telegram.methodID === 'GetUpdateCount') {
            this.widget.renderer._showBusyIndicator();
            //Start
            if (this.settings.counter <= this.telegram.data && (this.currentIteration === undefined || this.currentIteration === 0)) {
                this.startFetch();
            //If new alarm is set while fetching data    
            } else if (this.settings.counter < this.telegram.data && this.currentIteration > 0) {
                this.interruptFetch();

            //If we need to continue at page change
            //If counter == telegram.data and this.data == 0 then warning will appear!
            } else if (this.settings.counter === this.telegram.data && this.currentIteration > 0 && this.currentIteration < Math.ceil(this.telegram.data / this.settings.rowsPerFetch)) {
                this.pageChangeFetch();
                
            //Reset data if telegram data is 0
            } else if (this.telegram.data === 0) {
                this.resetFetch();
            } else {
                this.widget.renderer._hideBusyIndicator();
            }
        } else if (this.telegram.methodID === 'GetAlarmHistory') {
            this.getData();
        }
    };

    /**
     * @method getData
     * stores the data it has just recieved by calling the updateTable. It then increases the iteration, recalculates the to and from variables so that the next
     * iteration and sends a telegram to get the next data set. If there are no more datasets then it will call for a preparation of data before displaying it in
     * the DataTables.
     * @param {Object} telegram
     */
    p.getData = function (telegram) {
        if (this.getTo === 0 && this.currentIteration === 0) {
            this.data = [];
            this.currentData = [];
        }
        this.updateTable(this.telegram.data);
        this.currentIteration += 1;
        this.getFrom = _getRecordIndexFrom(this.currentIteration, this.settings.rowsPerFetch, this.settings.counter) - 1;
        this.getTo = _getRecordIndexTo(this.currentIteration, this.settings.rowsPerFetch, this.settings.counter);
        if (this.getFrom > 0) {
            this.sendData(this.getFrom, this.getTo);
        } else {
            this.widget.renderer._hideBusyIndicator();
        }
    };

    p.startFetch = function () {
        this.settings.counter = this.telegram.data;
        this.currentIteration = 0;
        this.getFrom = _getRecordIndexFrom(this.currentIteration, this.settings.rowsPerFetch, this.settings.counter) - 1;
        this.getTo = _getRecordIndexTo(this.currentIteration, this.settings.rowsPerFetch, this.settings.counter);
        this.sendData(this.getFrom, this.getTo);
    };

    p.interruptFetch = function () {
        this.getTo = _getRecordIndexFrom(0, this.settings.rowsPerFetch, this.settings.counter);
        this.getFrom = this.telegram.data;
        this.settings.counter = this.telegram.data;
        this.sendData(this.getFrom, this.getTo);
    };

    p.pageChangeFetch = function () {
        var ri = (this.telegram.data - this.data[this.data.length - 1]);
        this.currentIteration = Math.ceil(((ri !== undefined && !isNaN(ri)) ? ri.RecordIndex : 0) / this.settings.rowsPerFetch);
        this.getFrom = _getRecordIndexFrom(this.currentIteration, this.settings.rowsPerFetch, this.settings.counter) - 1;
        this.getTo = _getRecordIndexTo(this.currentIteration, this.settings.rowsPerFetch, this.settings.counter);
        if (this.getFrom > 0) {
            this.sendData(this.getFrom, this.getTo);
        } else {
            this.widget.renderer._hideBusyIndicator();
        }
    };

    p.resetFetch = function () {
        this.currentIteration = 0;
        this.getFrom = 0;
        this.getTo = 0;
        this.widget.settings.counter = 0;
        this.resetTable();
    };

    p.sendAckData = function (telegram, ackAllFlag) {
        this.ackAllFlag = (ackAllFlag !== undefined) ? ackAllFlag : false;
        this.widget.submitMpLinkData(telegram);
    };
    
    /**
     * @method getCategories
     * This method will send a telegram to the backend to get a list of all categories
     */
    p.getCategories = function () {
        this.widget.linkHandler.sendRequestAndProvideCallback('GetCategoryList', this.widget._updateData);
    };
    
    /**
     * @method sendData
     * From is the higher number, to is the lower number
     * @param {Integer} from the number from where in the dataset we get data
     * @param {Integer} to the number form where to in the dataset we get data
     */
    p.sendData = function (from, to) {
        this.widget.linkHandler.sendRequestAndProvideCallback('GetAlarmHistory', this.widget._updateData, undefined, { 'FromRecordIndex': from, 'ToRecordIndex': to });
    };
    
    function _getRecordIndexFrom(currentIteration, rowsPerFetch, counter) {
        return (counter - currentIteration * rowsPerFetch) + 1;
    }

    function _getRecordIndexTo(currentIteration, rowsPerFetch, counter) {
        var retVal = (counter - (currentIteration + 1) * rowsPerFetch) + 1;
        return (retVal <= 0) ? 0 : retVal;
    }

    p._addSVGToRow = function (sta) {

        if (isNaN(sta)) {
            //get nested number if anything else is sent in
            sta = parseInt(sta.match(/>[0-9]</)[0].match(/[0-9]/)[0]);
        }

        var img = '';
        if (sta === 1) {
            img = (this.widget.settings.imageActive === '') ? 'widgets/brease/AlarmHistory/img/act.svg' : this.widget.settings.imageActive;
        } else if (sta === 2) {
            img = (this.widget.settings.imageInactive === '') ? 'widgets/brease/AlarmHistory/img/inact.svg' : this.widget.settings.imageInactive;
        } else if (sta === 3) {
            img = (this.widget.settings.imageAcknowledged === '') ? 'widgets/brease/AlarmHistory/img/ack.svg' : this.widget.settings.imageAcknowledged;
        } else if (sta === 4) {
            img = (this.widget.settings.imageUnacknowledged === '') ? 'widgets/brease/AlarmHistory/img/unack.svg' : this.widget.settings.imageUnacknowledged;
        } 

        return "<img src='" + img + "' /><div class='stateIdent'>" + sta + '</div>';
    };

    /**
     * @method _fixCategory
     * @private
     * This method will change the category to the image path that the category will need where the image is stored.
     * Then add it to the image tag and return this.
     * @param {String} cat the category for a given row
     * @returns {String} Image tag in the format of a string to be used in the datatable
     */
    p._fixCategory = function (cat) {
        var img = '';
        if (brease.config.editMode) {
            img = '<img src="widgets/brease/AlarmHistory/img/' + cat + '.svg" /><div class="catIdent">' + cat + '</div>';
        } else if (cat.length > 0 && this.widget.settings.imagePrefix.length > 0) {
            img = '<img src="' + this.widget.settings.imagePrefix + cat + this.widget.settings.imageSuffix + '" /><div class="catIdent">' + cat + '</div>';
        } else if (cat.length > 0) {
            img = '<img src="" /><div class="catIdent">' + cat + '</div>';
        }
        return img;
    };

    /**
     * @method _mockEditorData
     * @private
     * @returns {Object} telegram for editor, not to be used in runtime
     */
    function _mockEditorData() {
        var mockData = { 
            'data': [
                { 'ins': 0, 'ad1': '', 'ad2': '', 'cat': 'cat0', 'cod': 10, 'mes': 'All these alarms are mocked and are in no way real', 'nam': 'Mocked Alarm', 'sco': '', 'sev': 1, 'stn': 2, 'sto': 1, 'tim': '1970-01-01T00:00:00.000Z' }, 
                { 'ins': 1, 'ad1': '', 'ad2': '', 'cat': 'cat1', 'cod': 2000, 'mes': 'Mocked:  Temperature level is critical ', 'nam': 'Mocked:  TemperatureCritical', 'sco': '', 'sev': 1, 'stn': 4, 'sto': 3, 'tim': '1970-01-01T11:33:15.141Z' }, 
                { 'ins': 2, 'ad1': '', 'ad2': '', 'cat': 'cat1', 'cod': 2001, 'mes': 'Mocked:  Temperature level is high', 'nam': 'Mocked:  TemperatureHigh', 'sco': '', 'sev': 2, 'stn': 2, 'sto': 1, 'tim': '1970-01-01T11:33:15.141Z' }, 
                { 'ins': 3, 'ad1': '', 'ad2': '', 'cat': 'cat2', 'cod': 0, 'mes': 'Mocked:  Initialization failed,  temperatureerror', 'nam': 'Mocked:  InitializationError', 'sco': 'gAxisBasic', 'sev': 2, 'stn': 3, 'sto': 2, 'tim': '1970-01-01T11:31:56.541Z' }, 
                { 'ins': 4, 'ad1': '', 'ad2': '', 'cat': 'cat2', 'cod': 0, 'mes': 'Mocked:  General axis error', 'nam': 'Mocked:  GeneralDriveError', 'sco': 'gAxisBasic', 'sev': 1, 'stn': 4, 'sto': 3, 'tim': '1970-01-01T11:31:56.341Z' }, 
                { 'ins': 5, 'ad1': '', 'ad2': '', 'cat': 'cat0', 'cod': 0, 'mes': 'Mocked:  Sequence import failed', 'nam': 'Mocked:  ImportFailed', 'sco': 'gSequencer', 'sev': 1, 'stn': 4, 'sto': 3, 'tim': '1970-01-01T11:31:55.741Z' }, 
                { 'ins': 6, 'ad1': '', 'ad2': '', 'cat': 'cat0', 'cod': 0, 'mes': 'Mocked:  Sequence import failed', 'nam': 'Mocked:  ImportFailed', 'sco': 'gSequencer', 'sev': 1, 'stn': 3, 'sto': 2, 'tim': '1970-01-01T11:31:55.865Z' }
            ], 
            'methodID': 'GetAlarmHistory', 
            'response': 'OK'
        };

        return mockData;
    }

    return ModelClass;

});
