define([
    'widgets/brease/common/libs/genericUnitTest/TestUtils/GenericUnitTestConstants',
    'widgets/brease/common/libs/genericUnitTest/TestUtils/GenericUnitTestUtils',
], function (GenericUnitTestConstants,GenericUnitTestUtils) {

    'use strict';

    var GenericUnitTestInit = {};

    function _initMut(Module, args, functionsToSpyOn){
        var moduleInstance,
            spyOnInitFunctions = jQuery.isEmptyObject(functionsToSpyOn);

        if(!spyOnInitFunctions){
            GenericUnitTestUtils.addFunctionsSpies(Module.prototype, functionsToSpyOn);
        }

        moduleInstance = new Module(args.arg1 , args.arg2, args.arg3, args.arg4, args.arg5, args.arg6, args.arg7 ,args.arg8);

        if(!spyOnInitFunctions){
            GenericUnitTestUtils.checkFunctionSpies(Module.prototype, functionsToSpyOn);
        }

        return moduleInstance;
    }

    function _initSubModules (module, subModules){
        for (var moduleName in subModules){

            if(subModules[moduleName].args === GenericUnitTestConstants.MUT){
                subModules[moduleName].args = module;
            }
            module[moduleName] = GenericUnitTestInit.initModule(subModules[moduleName]);
        }
    }

    GenericUnitTestInit.initModule = function(initParams){
        var mut = _initMut(initParams.module, initParams.args ,initParams.functionsToSpyOn);
        
            _initSubModules(mut, initParams.subModules);

        return mut;
    };

    return GenericUnitTestInit;

});