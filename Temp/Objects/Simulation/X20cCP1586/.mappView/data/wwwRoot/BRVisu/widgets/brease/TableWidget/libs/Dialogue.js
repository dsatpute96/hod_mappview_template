define([
    'brease/core/Class',
    'brease/events/BreaseEvent',
    'widgets/brease/GenericDialog/GenericDialog',
    'widgets/brease/GenericDialog/libs/config',
    'widgets/brease/Label/Label',
    'widgets/brease/CheckBox/CheckBox',
    'widgets/brease/DateTimeInput/DateTimeInput',
    'widgets/brease/Image/Image',
    'widgets/brease/Rectangle/Rectangle',
    'widgets/brease/DropDownBox/DropDownBox',
    'widgets/brease/NumericInput/NumericInput'
], function (SuperClass, BreaseEvent, Dialog, DialogConfig, Label, CheckBox, DateTimeIn, Image, Rectangle, DropDownBoxLegacy, NumIn) {
    
    'use strict';
    
    /** 
     * @class widgets.brease.TableWidget.libs.Dialogue
     * @extends brease.core.Class
     * @inheritable
     */

    var DialogueClass = SuperClass.extend(function Dialogue(widget) {
            this.dialog = new Dialog(widget.elem);// widget.configDiag;
            SuperClass.apply(this);
            this.widget = widget;
        }, null),

        p = DialogueClass.prototype;
    
    /**
     * @method initializeFilter
     * @param {String} lang
     * Should be implemented in the derived widgets, will start a dialog for the filtering
     */
    p.initializeFilter = function (lang) {
        //Implement in derived child widget
    };

    /**
     * @method initializeSort
     * @param {String} lang
     * Should be implemented in the derived widgets, will start a dialog for the sorting
     */
    p.initializeSort = function (lang) {
        //Implement in derived child widget
    };

    /**
     * @method initializeStyle
     * @param {String} lang
     * Should be implemented in the derived widgets, will start a dialog for the styling
     */
    p.initializeStyle = function (lang) {
        //Implement in derived child widget
    };

    /**
     * @method openFilter
     * This function will open the filtering dialogue and add eventlisteners to it
     */
    p.openFilter = function () {
        this.lang = _getLanguage();
        this.dialog.show(this.initializeFilter(this.lang), this.widget.elem);
        this.passedFirst = false;
        var self = this;
        this.dialog.isReady().then(function (arg) {
            $('#' + self.dialog.elem.id).on(BreaseEvent.WIDGET_READY, self._bind('_widgetAddedToFilter'));
            $('#' + self.dialog.elem.id).on('window_closing', self._bind('_collectFilterBeforeClosing'));
        });
    };

    /**
     * @method openSort
     * This function will open the sorting dialogue and add eventlisteners to it
     */
    p.openSort = function () {
        this.lang = _getLanguage();
        this.dialog.show(this.initializeSort(this.lang), this.widget.elem);
        this.passedFirst = false;
        var self = this;
        this.dialog.isReady().then(function (arg) {
            $('#' + self.dialog.elem.id).on(BreaseEvent.WIDGET_READY, self._bind('_widgetAddedToSort'));
            $('#' + self.dialog.elem.id).on('window_closing', self._bind('_collectSortBeforeClosing'));
        });
    };

    /**
     * @method openStyle
     * This function will open the styling dialogue and add eventlisteners to it
     */
    p.openStyle = function () {
        this.lang = _getLanguage();
        this.dialog.show(this.initializeStyle(this.lang), this.widget.elem);
        this.passedFirst = false;
        var self = this;
        this.dialog.isReady().then(function (arg) {
            $('#' + self.dialog.elem.id).on(BreaseEvent.WIDGET_READY, self._bind('_widgetAddedToStyle'));
            $('#' + self.dialog.elem.id).on('window_closing', self._bind('_collectStyleBeforeClosing'));
        });
    };

    /**
     * @method _initializeEmptyDialogConfig
     * @private
     * @param {String} headerText
     * This method will create the basic configuration needed for any dialog, based on the GenericDialog module.
     */
    p._initializeEmptyDialogConfig = function (headerText) {
        this.config = new DialogConfig();

        // dialog
        this.config.forceInteraction = true;
        this.config.contentWidth = 600;
        this.config.contentHeight = 480;

        // header
        this.config.header.text = headerText;

        //footer
        this.config.buttons.ok = true;
        this.config.buttons.cancel = true;
    };

    /**
     * @method _widgetAddedToFilter
     * @private
     * This method is used for recolouring the first widgets after these are initialized.
     */
    p._widgetAddedToFilter = function (e) {
        $('#' + this.dialog.elem.id).off(BreaseEvent.WIDGET_READY, this._bind('_widgetAddedToFilter'));
        this.filter.initialize();
        this.filter._reColourFirstLineSeparator();
    };

    /**
     * @method _widgetAddedToSort
     * @private
     * This method is used for recolouring the first widgets after these are initialized.
     */
    p._widgetAddedToSort = function (e) {
        $('#' + this.dialog.elem.id).off(BreaseEvent.WIDGET_READY, this._bind('_widgetAddedToSort'));
        this.sort.initialize();
        this.sort._reColourFirstLineSeparator();
    };

    /**
     * @method _widgetAddedToStyle
     * @private
     * This method is used for recolouring the first widgets after these are initialized.
     */
    p._widgetAddedToStyle = function (e) {
        $('#' + this.dialog.elem.id).off(BreaseEvent.WIDGET_READY, this._bind('_widgetAddedToStyle'));
        this.style.initialize();
    };

    /**
     * @method _collectFilterBeforeClosing
     * @private
     * This method will listen for the closing event and once it it is thrown it will collect the filter configuration, store it into the table
     * and redraw the table. Then it will update the backend that there was a change in the filter.
     */
    p._collectFilterBeforeClosing = function (e) {
        if (brease.uiController.parentWidgetId(e.target) === this.dialog.elem.id && this.dialog.getDialogResult() === 'ok') {
            this.widget.settings.config.filter = this.filter._widgetCollectStateBeforeClosing();
            this.widget.controller.draw();
            this.widget.sendFilterConfiguration();
        }
    };

    /**
     * @method _collectSortBeforeClosing
     * @private
     * This method will listen for the closing event and once it it is thrown it will collect the filter configuration, store it into the table
     * and redraw the table. Then it will update the backend that there was a change in the filter.
     */
    p._collectSortBeforeClosing = function (e) {
        if (brease.uiController.parentWidgetId(e.target) === this.dialog.elem.id && this.dialog.getDialogResult() === 'ok') {
            this.widget.settings.config.sort = this.sort._widgetCollectStateBeforeClosing();
            this.widget.controller.draw();
            this.widget.sendSortConfiguration();
        }
    };

    /**
     * @method _collectStyleBeforeClosing
     * @private
     * This method will listen for the closing event and once it it is thrown it will collect the styling configuration, store it into the table
     * and redraw the table. Then it will update the backend that there was a change in the style.
     */
    p._collectStyleBeforeClosing = function (e) {
        if (brease.uiController.parentWidgetId(e.target) === this.dialog.elem.id && this.dialog.getDialogResult() === 'ok') {
            this.widget.settings.config.style = this.style._widgetCollectStateBeforeClosing();
            this.widget.controller.draw();
            this.widget.sendStyleConfiguration();
        }
    };
    
    /**
     * @method _addRowHandler
     * @private
     * @deprecated
     */
    p._addRowHandler = function (e) {
        //Function to switch between the different tabs avaible for modularity
        this.filter._addRowHandler(e);
    };
    
    /**
     * @method _getLanguage
     * @private
     * This method will determine which langauge is currenly used. If it's not german it will default to English.
     */
    function _getLanguage() {
        var lang = brease.language.getCurrentLanguage();
        if (lang !== 'de') {
            lang = 'en';
        }
        return lang;
    }

    /**
     * @method _reColourAllObjects
     * @private
     * @param {Object} self 
     * Will recolor all separators (?)
     */
    function _reColourAllObjects(self) {
        self.filter._reColourFirstLineSeparator();
    }

    /**
     * @method dispose
     * This method will dispose of/remove the dialog.
     */
    p.dispose = function () {
        this.dialog.dispose();
        SuperClass.prototype.dispose.call(this);
    };

    return DialogueClass;
});
