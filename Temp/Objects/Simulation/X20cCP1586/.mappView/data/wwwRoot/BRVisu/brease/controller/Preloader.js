define(['brease/enum/Enum', 'brease/events/BreaseEvent', 'brease/model/VisuModel', 'brease/core/Utils', 'brease/controller/WidgetController', 'brease/controller/objects/AssignTypes'], 
    function (Enum, BreaseEvent, visuModel, Utils, widgetController, AssignTypes) {

        'use strict';

        var Preloader = function (rootContainer, loaderPool, splashScreenEl, bootProgressBar) {
                this.rootContainer = rootContainer;
                this.loaderPool = loaderPool;
                this.splashScreenEl = splashScreenEl || $('#splashscreen');
                this.embeddedVisus = [];
                this.contentsToPreload = this.getContentsToPreload($.extend({}, visuModel.allContents()));
                this.contentsToPreloadLength = 0;
                this.rejectedContents = [];
                this.bootProgressBar = bootProgressBar;
                this.readyDef = $.Deferred();
                this.timeoutForSuspended = defaults.timeoutForSuspended;
                this.maxAllowedLoadingTime = defaults.maxAllowedLoadingTime;
                this.tmpQueue = [];
            },
            defaults = {
                maxAllowedLoadingTime: 16000, // has to be greater than the timeout of the server (=15000)
                timeoutForSuspended: 5000,
                showContentLoadedMessage: 500,
                preloadingContainerId: 'preloadingContainer',
                progressBarInitialWidth: 100,
                progressbarSteps: 0,
                progressBarWrapperClass: 'system_brease_StartupProgressBar_style_default startupProgressBarWrapper',
                progressBarClass: 'startupProgressBar',
                preloadingInfoWrapperClass: 'preLoadingInfoWrapper',
                preloadingInfoClass: 'preLoadingInfo'
            },
            preLoadingState = { 'WORK_IN_PROGRESS': 0.5, 'CONTENTLOADER_INITIALIZED': 1, 'FINISHED': 2 },
            p = Preloader.prototype;

        p.init = function () {
            var deferred = new $.Deferred(),
                self = this;
            // has to be !== undefined in LoaderPool
            this.loaderPool.contentsToLoad = [];

            // calculate the size of the steps that the preloader should move
            this.contentsToPreloadLength = Object.keys(this.contentsToPreload).length;
            defaults.progressbarSteps = 100 / (this.contentsToPreloadLength - 1);

            // copy splashscreen and add a progressBar
            _setPreloadingInfoScreen.call(this);

            // append a hidden preloadingContainer
            this.preloadingContainer = $('<div id="' + defaults.preloadingContainerId + '"></div>');
            this.preloadingContainer.appendTo('body');

            _addContentsToPreloadToStack.call(this);

            // activate embedded visu, so that contents can be loaded and cached
            var deferredEmbeddedVisusFlatList = [];
            this.embeddedVisus.map(function (visu) {
                var activateDeferred = _activateVisu.call(self, visu.id);
                activateDeferred.promise();
                deferredEmbeddedVisusFlatList.push(activateDeferred);
            });
            $.when.apply($, deferredEmbeddedVisusFlatList).then(function () {
                deferred.resolve();
            });

            return deferred;
        };

        p.startProcessingContentsToPreloadFromStack = function () {
            var deferred = $.Deferred(),
                self = this,
                contentToPreload = this.tmpQueue.shift();

            if (contentToPreload) {
                
                _updatePreloadingCount.call(this, this.tmpQueue.length + 1, contentToPreload.id);
                this.contentsToPreload[contentToPreload.id].preLoadingState = preLoadingState.WORK_IN_PROGRESS;

                // create new contentLoader widget via loaderPool
                // deferred resolved in _initializedHandler in LoaderPool
                this.loaderPool.createNew(this.preloadingContainer.get(0), contentToPreload, deferred);

                $.when(deferred).done(function (loaderId, success) {
                    if (success) {
                        var _contentLoader;
                        try {
                            _contentLoader = widgetController.callWidget(loaderId, 'widget');
                        } catch (e) {
                            Utils.logError(e);
                        }

                        // listen to content activated event
                        // and then check if widgets are ready
                        if (_contentLoader) {
                            contentActivatedListener.call(self, _contentLoader.settings.contentId, loaderId).done(function (contentId, loaderId) { 
                                widgetsReadylistener.call(self, loaderId); 
                            });

                            self.contentsToPreload[_contentLoader.settings.contentId].preLoadingState = preLoadingState.CONTENTLOADER_INITIALIZED;
                        }
                    } else {
                    // console.log('creation of contentLoader widget failed');
                    // try again to create a contentLoader widget
                        this.tmpQueue.push(contentToPreload);
                    }
                });
            } else {
            //console.log('preloaderItem stack is empty');
            }

            return this.readyDef.promise();
        };
    
        function widgetsReadylistener(loaderId) {
            var _contentLoader = widgetController.callWidget(loaderId, 'widget'),
                self = this;

            // there are widgets that fire the ready event to quickly, so they are allready fired and so we can not listen to it
            // only listen to those, that are not ready and due to this will fire the ready event
            var widgetsOfContent = widgetController.getWidgetsOfContent(_contentLoader.settings.contentId, Enum.WidgetState.IN_QUEUE),
                widgetsInElem = widgetController.findWidgets(_contentLoader.elem, false, 0),
                allWidgets = Utils.uniqueArray(widgetsOfContent.concat(widgetsInElem));

            var filteredArr = allWidgets.filter(function (id) {
                return widgetController.getState(id) !== Enum.WidgetState.READY;
            });
            // console.log('widgetsReadylistener:' + _contentLoader.settings.contentId + ':' + JSON.stringify(filteredArr));
            // console.log('findWidgets:' + _contentLoader.settings.contentId + ':' + JSON.stringify(allWidgets));

            var allWidgetsReady = function () {
                _addToLoaderPool.call(self, _contentLoader);
            };

            var notAllWidgetsReady = function (contentId) {
                self.rejectedContents.push(contentId);
                _disposeLoader.call(self, _contentLoader);
            };

            //handle situation if all widgets were ready
            if (filteredArr.length === 0) {
                allWidgetsReady();
            } else {
            //add eventListener
                _addWidgetReadyEventListener.call(self, filteredArr, _contentLoader)
                    .done(function () { allWidgetsReady(); })
                    .fail(function (contentId, aUnprocessedWidgets) { notAllWidgetsReady(contentId, aUnprocessedWidgets); });
            }
        }

        function contentActivatedListener(contentId, loaderId) {
            var deferred = new $.Deferred();
            var eventListener = function (e) {
                if (e.detail.contentId === contentId) {
                    document.body.removeEventListener(BreaseEvent.CONTENT_ACTIVATED, eventListener);
                    clearTimeout(failureTimeout);
                    deferred.resolve(contentId, loaderId);
                }
            };
            var failureTimeout = setTimeout(function () {
                //console.log('TIMED OUT:' + BreaseEvent.CONTENT_ACTIVATED);
                document.body.removeEventListener(BreaseEvent.CONTENT_ACTIVATED, eventListener);
                deferred.resolve(contentId, loaderId);
            }, this.maxAllowedLoadingTime);

            document.body.addEventListener(BreaseEvent.CONTENT_ACTIVATED, eventListener);
            return deferred;
        }

        // strip out contents that have no preloading flag
        // integrate contents of the startpage at the beginning
        p.getContentsToPreload = function (visuDataContents) {
            var startPage = visuModel.getPageById(visuModel.startPageId),
                startPageContents = visuModel.getContentsOfPage(startPage.id, startPage.type),
                filteredContents = [],
                filteredContentsObject = {},
                contentId;

            this.embeddedVisus = getEmbeddedVisus(visuModel.getVisuData());

            // strip out every content that has not a preCache flag in the configurations
            // FYI: beware if in .vis file content caching is turned on for all contents, every content gets the flag from AS
            for (contentId in visuDataContents) {
                if (visuDataContents[contentId] !== undefined) {
                    if (visuDataContents[contentId].configurations === undefined || visuDataContents[contentId].configurations.hasOwnProperty('preCache') === false || visuDataContents[contentId].configurations.preCache === false) {
                        delete visuDataContents[contentId];
                    }
                }
            }

            // return an ordered contentList, where the contents of the startPage come first
            // -> so they get preloaded first
            startPageContents.map(function (id) {
                var content = visuDataContents[id];
                if (content !== undefined) {
                    filteredContents.unshift(content);
                }
            });

            //add contents of embedded visus on the startPage
            if (this.embeddedVisus.length > 0) {
                var contentsInVisuEmbeddedOnStartpage = [];
                this.embeddedVisus.map(function (visu) {
                // Todo: assure the order and priorize contents from a visu embedded in the startpage
                    if (visu.embeddedOnStartPage === true) {
                        var contentsInVisu = [];
                        var contents = visuModel.getVisuData().contents;
                        for (contentId in contents) {
                            var content = contents[contentId];
                            if (content.configurations && content.configurations.hasOwnProperty('preCache') === true) {
                                if ((content.visuId === visu.id) && (content.configurations.preCache === true)) {
                                    contentsInVisu.push(content);
                                }
                            }

                        }
                        contentsInVisuEmbeddedOnStartpage = contentsInVisuEmbeddedOnStartpage.concat(contentsInVisu);
                    }
                });

                filteredContents = filteredContents.concat(contentsInVisuEmbeddedOnStartpage);
            }

            // add other contents to my filteredContents, with startPageContents stripped out
            Object.keys(visuDataContents).filter(function (contentId) {
                return !startPageContents.includes(contentId);
            }).map(function (id) {
                var content = visuDataContents[id];
                filteredContents.push(content);
            });
            
            if (filteredContents.length > this.loaderPool.maxSlots) {
                brease.loggerService.log(Enum.EventLoggerId.CLIENT_PRECACHING_CONTENT_SLOTS_EXCEEDED, Enum.EventLoggerCustomer.BUR, Enum.EventLoggerVerboseLevel.OFF, Enum.EventLoggerSeverity.WARNING, []);
                // limit array/object of contents that should get cached to the limit of the slots
                filteredContents = filteredContents.slice(0, this.loaderPool.maxSlots);
            }

            // Todo: leave it as an array --> so that no extra transformation is needed
            // transform into an object
            filteredContents.map(function (content) {
                if (content) {
                    filteredContentsObject[content.id] = content;
                }
            });

            return filteredContentsObject;
        };

        p.getTmpQueue = function () {
            return this.tmpQueue;
        };

        function _addWidgetReadyEventListener(arr, _contentLoader) {
            var deferred = new $.Deferred();
            var eventListenerContainer = [];
            //console.log('_addWidgetReadyEventListener:', arr);

            arr.map(function (widgetId) {
                var widgetElem = document.getElementById(widgetId);

                var eventListener = function (e) {
                    //console.log(e);
                    if (e.target.id === widgetElem.id) {
                        widgetElem.removeEventListener(BreaseEvent.WIDGET_READY, eventListener);
                        arr.splice(arr.indexOf(e.target.id), 1);
                        if (arr.length <= 0) {
                        // wait a tick
                            setTimeout(function () {
                                clearTimeout(loadingTimeExceededTimer);
                                deferred.resolve();
                            }, 0);
                        } else {
                        // watch arr decrease :-)
                        //console.log(arr.length);
                        }
                    }
                };

                if (widgetElem) {
                    widgetElem.addEventListener(BreaseEvent.WIDGET_READY, eventListener);
                    eventListenerContainer.push({ widget: { elem: widgetElem }, eventListener: eventListener });
                }

            });

            // content has 10 seconds (see defaults.maxAllowedLoadingTime) to load all widgets
            // workaround if a widget does not fire the ready event, wait for n seconds then reject and claim all widgets of content have finished
            var loadingTimeExceededTimer = setTimeout(function () {
                //console.log('TIMED OUT:' + BreaseEvent.WIDGET_READY);
                eventListenerContainer.map(function (item) {
                    if (item !== undefined && item.widget !== null) {
                        item.widget.elem.removeEventListener(BreaseEvent.WIDGET_READY, item.eventListener);
                    }
                });
                deferred.reject(_contentLoader.settings.contentId, eventListenerContainer);
            }, this.maxAllowedLoadingTime); //10000

            return deferred;
        }

        function _disposeLoader(contentLoader) {
            this.loaderPool.flushLoader(contentLoader.elem);
            _continueWithStack.call(this, contentLoader.settings.contentId);
        }

        function _addToLoaderPool(contentLoader) {
            var self = this,
                contentId = contentLoader.settings.contentId,
                suspendedListener = function (e) {
                    if (e.detail.contentId === contentId) {
                        window.clearTimeout(timeout);
                        document.body.removeEventListener(BreaseEvent.CONTENT_DEACTIVATED, suspendedListener);
                        _continueWithStack.call(self, contentId); 
                    }
                }, 
                timeout = window.setTimeout(function () {
                    //console.log('TIMED OUT:' + BreaseEvent.CONTENT_DEACTIVATED);
                    document.body.removeEventListener(BreaseEvent.CONTENT_DEACTIVATED, suspendedListener);
                    _continueWithStack.call(self, contentId); 
                }, self.timeoutForSuspended);

            document.body.addEventListener(BreaseEvent.CONTENT_DEACTIVATED, suspendedListener);
            self.loaderPool.suspendLoader(contentLoader.elem);
        }

        function _continueWithStack(contentId) {

            if (this.tmpQueue.length !== 0) {
                this.contentsToPreload[contentId].preLoadingState = preLoadingState.FINISHED;
                // iterate until stack is empty
                this.startProcessingContentsToPreloadFromStack();
            } else {
                _updatePreloadingCount.call(this, 0);
            }
        }

        function _addContentsToPreloadToStack() {
            for (var contentId in this.contentsToPreload) {
                this.tmpQueue.push(this.contentsToPreload[contentId]);
            }
        }

        function _setPreloadingInfoScreen() {
        // set splashscreen again to mask startPage
        // not needed if precaching is handled before loading of startPage
            $(this.rootContainer).append(this.splashScreenEl);

            if (this.bootProgressBar) {
                var preloadingInfoWrapperEl = $('<div class="' + defaults.preloadingInfoWrapperClass + '"></div>');
                defaults.preloadingInfo = $('<div class="' + defaults.preloadingInfoClass + '"><span>' + this.contentsToPreloadLength + '</span> contents to preload: <div style="display: inline-block" class="' + defaults.preloadingInfoClass + '_inner"></div></div>');
                preloadingInfoWrapperEl.append(defaults.preloadingInfo);

                defaults.preloadingInfoSpan = $('span', defaults.preloadingInfo).eq(0);
                defaults.preloadingInfoInner = $('.' + defaults.preloadingInfoClass + '_inner', defaults.preloadingInfo).eq(0);

                // add a progressbar
                _initProgressBar.call(this, preloadingInfoWrapperEl);
            }
        }

        function _initProgressBar(preloadingInfoWrapperEl) {
            var startupProgressBarWrapperEl = $('<div class="' + defaults.progressBarWrapperClass + '"></div>');
            defaults.startupProgressBarEl = $('<div class="' + defaults.progressBarClass + '"></div>');

            startupProgressBarWrapperEl.prepend(preloadingInfoWrapperEl);
            startupProgressBarWrapperEl.append(defaults.startupProgressBarEl);

            this.splashScreenEl.prepend(startupProgressBarWrapperEl);
        }

        function _updatePreloadingCount(count, contentId) {
            var self = this;

            if (self.bootProgressBar) {
                defaults.preloadingInfoSpan.html(count);
                if (contentId !== undefined) {
                    defaults.preloadingInfoInner.html(contentId);
                }
                _moveProgressBar();
            }

            if (count === 0) {
                delete self.rejectedContents;
                if (self.bootProgressBar) {
                    defaults.preloadingInfo.html('All contents have been precached'); //in ' + (+(Math.round(seconds + 'e+2') + 'e-2')) + ' seconds'
                    defaults.startupProgressBarEl.slideUp('slow');
                }
                // just a helper timer. The user should be able to see and recognize the info message, that everything is ready
                setTimeout(function () {
                    self.readyDef.resolve();
                    $('#' + defaults.preloadingContainerId).remove();
                    $('#splashscreen').remove();
                }, defaults.showContentLoadedMessage);
            }
        }

        function _moveProgressBar() {
            defaults.progressBarInitialWidth -= defaults.progressbarSteps;
            defaults.startupProgressBarEl.width(defaults.progressBarInitialWidth + '%');
        }

        function findEmbeddedVisus(page, visuData, isStartPage) {
            var embeddedVisus = [];

            for (var assignmentId in page.assignments) {
                var assignment = page.assignments[assignmentId];
                if (assignment.type === AssignTypes.VISU && visuData.visus[assignment.contentId] !== undefined) {
                    var visuId = assignment.contentId = Utils.ensureVisuId(assignment.contentId);
                    var visu = visuModel.getVisuById(visuId);
                    var myVisuObj = $.extend({}, visu, { page: page, assignment: assignment });

                    if (isStartPage) {
                        myVisuObj.embeddedOnStartPage = true;
                        embeddedVisus.unshift(myVisuObj);
                    } else {
                        embeddedVisus.push(myVisuObj);
                    }
                }
            }

            return embeddedVisus;
        }

        function removeDuplicates(myArr, prop) {
            return myArr.filter(function (obj, pos, arr) {
                return arr.map(function (mapObj) {
                    return mapObj[prop];
                }).indexOf(obj[prop]) === pos;
            });
        }

        function getEmbeddedVisus(visuData) {
            
            var embeddedVisus = [];

            // iterate through pages and search for embedded visus
            for (var pageId in visuData.pages) {
                var isStartPage = (pageId === visuData.startPageId),
                    page = visuData.pages[pageId],
                    embeddedVisusInPages = findEmbeddedVisus(page, visuData, isStartPage);
                embeddedVisus = embeddedVisus.concat(embeddedVisusInPages);
            }
            // iterate through dialogs and search for embedded visus
            for (var dialogId in visuData.dialogs) {
                var dialog = visuData.dialogs[dialogId],
                    embeddedVisusInDialogs = findEmbeddedVisus(dialog, visuData, false);
                embeddedVisus = embeddedVisus.concat(embeddedVisusInDialogs);
            }

            embeddedVisus = removeDuplicates(embeddedVisus, 'id');
            return embeddedVisus;
        }

        function _activateVisu(visuId) {
            var deferred = $.Deferred();
            var _activateEmbeddedVisuSuccess = function () {
                deferred.resolve();
            };

            var _activateEmbeddedVisuFailed = function () {
                deferred.reject();
            };

            $.when(visuModel.activateVisu(visuId, {
                visuId: visuId
            })).then(_activateEmbeddedVisuSuccess, _activateEmbeddedVisuFailed);

            return deferred;
        }

        return Preloader;
    });
