define([], function () {

    'use strict';

    var GenericUnitTestConstants = {
        //SetUp Constants
        BINDING: "BINDING", //data located in "this.bindings[dataName] = setUpData"
        BREASE_CONFIG: "BREASE_CONFIG", //data located in "brease.config[dataName] = setUpData"
        SETTING: "SETTING", //data located in "this.settings[dataName]"
        DATA: "DATA", //data located in "this[dataName] = setUpData"
        EL:"EL", //data located in "this.el = setUpData"
        ELEM:"ELEM", //data located in "this.elem = setUpData"

        //used machters
        CHECK_FUNCTION_BINDING: "CHECK_FUNCTION_BINDING", //check if a function was correct bound using "this._bind()""
        COMPARE_JQUERYOBJECT: "COMPARE_JQUERYOBJECT", //check if the returned Jqerry hast he same id,class and tag
        
        //spy parents
        CONSOLE: "CONSOLE", //the spied function is a console function
        EVENT: "EVENT", //the spied function is part of an event
        JQUERY:"JQUERY", //the spied function is part of "jqerry"
        JQUERY_PROTOTYPE :"JQUERY_PROTOTYPE", //the spied function is part of the "jqerry.prototype"
        MUT: "MUT", //the spied function is part the module under test
        OBJECT: "OBJECT", //the spied function is part of "Object"
        STAND_ALONE: "STAND_ALONE", //the spied function is part of an standalone object use the "parentModule" property to hand over the module
        SUBMODULE: "SUBMODULE", //the spied function is part of the module under test, make sure the module is also instanziated
        BASEWIDGET:"BASEWIDGET", //the spied function is part of the "BaseWidget"
        BREASE_UICONTROLLER: "BREASE_UICONTROLLER", //the spied function is part of "brease.uiController"

        //MOCKS
        EVENTMOCK: {dispatch :function(){return "EVENTMOCK";}}, //event mock to verify if event was dispatched
    };

    return GenericUnitTestConstants;

});