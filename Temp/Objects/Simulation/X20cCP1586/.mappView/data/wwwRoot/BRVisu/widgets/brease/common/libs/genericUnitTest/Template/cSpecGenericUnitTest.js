define([
    'widgets/brease/common/libs/genericUnitTest/TestUtils/GenericUnitTestConstants',
    'brease/events/BreaseEvent',
], function (GenericUnitTestConstants,BreaseEvent) {

    'use strict';

    var cSpec = {
        run :true,
        init:{}, // data for the init of the module under test
        functions:{}, //data for the functions to test in the module
    };

    cSpec.init = {
        run: true,
        args:[undefined, undefined, true], //arguments with wich the module will be instanziated
        module: undefined, //the module under test, needs to be defined!
        functionsToSpyOn:{}, // functions which will be called on instanziating the module
        subModules:{ //sub modules which also needs to be instanziated.
            nameOftheSubModule:{ //the name of the subModule, this needs to be identical with the name it has in the Widget code eg.: this.mpLinkHandler
                args: [], //arguments with wich the sub module will be instanziated
                module: undefined, //the sub module itself
                functionsToSpyOn:{ // functions which will be called on instanziating the sub module
                    functionName:{ 
                        parentModuleType:GenericUnitTestConstants.MUT, //the type of the parentModuleType where the function to spy on is located
                        parentModuleName:undefined, //the name of the parentModule if the parentModuleType is a sub module of the mut
                        parentModule:undefined, //the instance of the parentModule if the parentModuleType is standalone
                        args: [ //the arguments which the spied function should have been called
                            []//every array is one call of the spied function
                        ],
                        callCount: 0, //callcount of the spied function
                        return: undefined //the mocked return value of the spied function.
                    },
                },
                subModules:{} //if this submodules needs additinonal sub Module the need to be defined here
            },
        }
    };
    
    cSpec.functions = {
        run:true,
        functionUnderTest:{
            functionUnderTestName:{
                run:true,
                testCases:[
                    [
                        "add a description here!",
                        {   
                            setUpData:{ //data which needs to be adjusted before so the test can be performed. eg: brease.config.editMode
                                dataName:{ //the name of the data which needs to be set up
                                    type:GenericUnitTestConstants.BREASE_CONFIG, // where the data is located.
                                    value: undefined //the vale which should be set before the test
                                },
                            }, 
                            functionsToSpyOn:{ //functions which are called in the function under test, spies will be added automatical to this function
                                functionName:{ 
                                    matcher: undefined, // matcher used to verify the return value, if undefinded "toEqual" is used
                                    parentModuleType:GenericUnitTestConstants.MUT, //the type of the parentModuleType where the function to spy on is located
                                    parentModuleName:undefined, //the name of the parentModule if the parentModuleType is a sub module of the mut
                                    parentModule:undefined, //the instance of the parentModule if the parentModuleType is standalone
                                    args: [ //the arguments which the spied function should have been called
                                        []//every array is one call of the spied function
                                    ],
                                    callCount: 0, //callcount of the spied function
                                    return: undefined //the mocked return value of the spied function.
                                }, 
                            },
                            args:[],//arguments for the funtion under test
                            return:undefined //return value of the funtion under test
                        }
                    ], 
                ]
            }
        }
    };

    return cSpec;
});