﻿define([], function () {
    'use strict';
    function DialogueTexts() {
        return {
            en: {
                title: 'Configuration dialogue for AlarmList',
                filter: {
                    inactive: 'Inactive',
                    active: 'Active',
                    state: 'and state is'
                },
                style: {
                    title: 'Styling',
                    act: 'Active',
                    inact: 'Inactive',
                    actack: 'Active Acknowledge',
                    any: 'Any',
                    sev: 'And severity condition',
                    and: 'and',
                    or: 'or',
                    style: 'Set style',
                    state: 'if alarm is'
                }
            },
            de: {
                title: 'Konfigurationsdialog für AlarmList',
                filter: {
                    inactive: 'Inaktiv',
                    active: 'Aktiv',
                    state: 'und Zustand ist'
                },
                style: {
                    title: 'Styling',
                    act: 'Aktiv',
                    inact: 'Inaktiv',
                    actack: 'Aktiv & Bestätigen',
                    any: 'Alle',
                    sev: 'und Schweregrad',
                    and: 'und',
                    or: 'oder',
                    style: 'Style festlegen',
                    state: 'wenn Alarm'
                }
            }
        };
    }
    
    return new DialogueTexts();
});
