define([
    'brease/core/BaseWidget',
    'widgets/brease/common/libs/genericUnitTest/TestUtils/GenericUnitTestConstants',
], function (BaseWidget, GenericUnitTestConstants) {

    'use strict';

    var GenericUnitTestUtils = {};
    GenericUnitTestUtils.originalData = {};

    function _getModule (mut, moduleType, moduleName, parentmodule){
        var selectedModule;
        switch (moduleType) {
            case GenericUnitTestConstants.MUT:       
                selectedModule = mut;
                break;
            case GenericUnitTestConstants.JQUERY:
                selectedModule = $;
                break;
            case GenericUnitTestConstants.JQUERY_PROTOTYPE:
                selectedModule = $.fn;
                break;  
            case GenericUnitTestConstants.CONSOLE:
                selectedModule = console;
                break;
            case GenericUnitTestConstants.OBJECT:
                selectedModule = Object;
                break;
            case GenericUnitTestConstants.EVENT:
                selectedModule = GenericUnitTestConstants.EVENTMOCK;
                break;
            case GenericUnitTestConstants.BASEWIDGET:
                selectedModule = BaseWidget.prototype;
                break;
            case GenericUnitTestConstants.BREASE_UICONTROLLER:
                selectedModule = brease.uiController;
                break;
            case GenericUnitTestConstants.SUBMODULE:
                selectedModule = mut[moduleName];
                if(selectedModule === undefined){
                    console.iatWarn("no Module with type :'" + moduleName +"' found in module under test. check init data");
                }
                break;
            case GenericUnitTestConstants.STAND_ALONE:
                selectedModule = parentmodule;
                if(selectedModule === undefined){
                    console.iatWarn("no Module standalone Module found. check cspec if module was defined");
                }
                break;
            default:
                console.iatWarn("unknowen moduletype set:'"+ moduleType +"'!" );
        }

        return selectedModule;
    }

    function _getMatcher(matcherName){
        var matcher;
        switch (matcherName) {
            case GenericUnitTestConstants.COMPARE_JQUERYOBJECT:
                matcher = "compareJQUERYObject";
                break;
            case GenericUnitTestConstants.CHECK_FUNCTION_BINDING:
                matcher = "checkFunctionBinding";
                break;
            default:
                matcher = "toEqual";
        }
        return matcher;
    }

    GenericUnitTestUtils.addFunctionsSpies = function(mut,testFunction){
        var testFunctionName,
            testFunctionObj,
            moduleToSpyOn;

        for (testFunctionName in testFunction) {
            if (testFunction.hasOwnProperty(testFunctionName)) {
                testFunctionObj = testFunction[testFunctionName];
                moduleToSpyOn = _getModule(mut, testFunctionObj.parentModuleType, testFunctionObj.parentModuleName, testFunctionObj.parentModule);

                spyOn(moduleToSpyOn, testFunctionName).andReturn(testFunctionObj.return);
            }
        }
    };

    GenericUnitTestUtils.checkFunctionSpies = function(mut,testFunction){
        var testFunctionObj,
            testFunctionSpy,
            testFunctionName,
            moduleToSpyOn,
            matcher;

        for (testFunctionName in testFunction) {
            if (testFunction.hasOwnProperty(testFunctionName)) {
                testFunctionObj = testFunction[testFunctionName];
                moduleToSpyOn = _getModule(mut, testFunctionObj.parentModuleType, testFunctionObj.parentModuleName, testFunctionObj.parentModule); 
                testFunctionSpy = moduleToSpyOn[testFunctionName];

                expect(testFunctionSpy.callCount).toBe(testFunctionObj.callCount);

                for (var i = 0 ; i < testFunctionObj.callCount; i += 1) {
                    matcher =  _getMatcher(testFunctionObj.matcher);
                    
                    expect(testFunctionSpy.calls[i].args)[matcher](testFunctionObj.args[i]);
                }
            }
        }
    };

    GenericUnitTestUtils.testSetup = function(mut, setUpData){
        for(var entry in setUpData){
            switch (setUpData[entry].type) {
                case GenericUnitTestConstants.BREASE_CONFIG:
                    GenericUnitTestUtils.originalData[entry] = brease.config[entry];
                    brease.config[entry] = setUpData[entry].value;
                    break;
                case GenericUnitTestConstants.SETTING:
                    GenericUnitTestUtils.originalData[entry] = mut.settings[entry];
                    mut.settings[entry]= setUpData[entry].value;
                    break;
                case GenericUnitTestConstants.BINDING:
                    GenericUnitTestUtils.originalData["bindings"] = mut.bindings;
                    mut.bindings = {};
                    mut.bindings[entry] = setUpData[entry].value;
                    break;
                case GenericUnitTestConstants.DATA:
                    GenericUnitTestUtils.originalData[entry] = mut[entry];
                    mut[entry] = setUpData[entry].value;
                    break;
                case GenericUnitTestConstants.EL:
                    mut.el = setUpData[entry].value;
                    break;
                case GenericUnitTestConstants.ELEM:
                    mut.elem = setUpData[entry].value;
                    break;
                default:
                    return undefined;
            }
        }
    };

    GenericUnitTestUtils.testTearDown = function(mut, setUpData){
        for(var entry in setUpData){
            switch (setUpData[entry].type) {
                case GenericUnitTestConstants.BREASE_CONFIG:       
                    brease.config[entry] = GenericUnitTestUtils.originalData[entry];
                    break;
                case GenericUnitTestConstants.SETTING:
                    mut.settings[entry] = GenericUnitTestUtils.originalData[entry];
                    break;
                case GenericUnitTestConstants.BINDING:
                    mut.bindings = GenericUnitTestUtils.originalData["bindings"];
                    break;
                case GenericUnitTestConstants.DATA:
                    mut[entry] = GenericUnitTestUtils.originalData[entry];
                    break;
                case GenericUnitTestConstants.EL:
                    mut.el = undefined;
                    break;
                case GenericUnitTestConstants.ELEM:
                    mut.elem = undefined;
                    break;
                default:
                    return undefined;
            }
        }
        GenericUnitTestUtils.originalData = {};
    };

    return GenericUnitTestUtils;

});