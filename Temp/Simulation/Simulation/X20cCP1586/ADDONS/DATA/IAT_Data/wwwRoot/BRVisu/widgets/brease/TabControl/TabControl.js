/*global define,brease,console,CustomEvent,_*/
define(['brease/core/ContainerWidget',
    'brease/events/BreaseEvent',
    'brease/enum/Enum',
    'brease/decorators/DragAndDropCapability'], function (SuperClass, BreaseEvent, Enum, dragAndDropCapability) {

    'use strict';

    var uiController = brease.uiController,

	/**
	* @class widgets.brease.TabControl
	* #Description
	* widget which controls a set of TabItems
    *
    * @mixins widgets.brease.common.DragDropProperties.libs.DroppablePropertiesEvents
	*   
	* @breaseNote  
	* @extends brease.core.ContainerWidget
	* @iatMeta studio:isContainer
	* true
	*
	* @iatMeta category:Category
	* Container
	* @iatMeta description:short
	* Kontrolliert eine Menge an TabItems
	* @iatMeta description:de
	* Container, welcher TabItems verwaltet und steuert
	* @iatMeta description:en
	* Container which controls TabItem Widgets
	*/

	/**
    * @property {WidgetList} [children=["widgets.brease.TabItem"]]
    * @inheritdoc  
    */
	/**
	* @cfg {Integer} selectedIndex=0
	* @iatStudioExposed
    * @iatCategory Data
	* @bindable
    * Index of the selected item
	*/

	/**
	* @cfg {brease.enum.TabPosition} tabPosition='top'
	* @iatStudioExposed
    * @iatCategory Appearance
	* Defines where the Tab-Buttons should be placed in relation to the Container.  
	*/


	defaultSettings = {
	    selectedIndex: 0,
	    oldIndex: 0,
	    tabPosition: 'top'
	},

	WidgetClass = SuperClass.extend(function TabControl() {
	    SuperClass.apply(this, arguments);
	}, defaultSettings),

	p = WidgetClass.prototype;

    p.init = function () {

        if (this.settings.omitClass !== true) {
            this.addInitialClass('breaseTabControl');
        }
        if (brease.config.editMode === true) {
            this.settings.height = this.el.height();
            this.settings.width = this.el.width();
            this.addInitialClass('iatd-outline');
            _createTabItemAddControl(this);

        }

        SuperClass.prototype.init.call(this);
        _setClasses(this);
        _findTabs(this);
    };

    /**
    * @method setSelectedIndex
    * @iatStudioExposed
    * Sets SelectedIndex
    * @param {Integer} value
    */
    p.setSelectedIndex = function (value) {
        this.settings.selectedIndex = value;

        this.showHideContents(value);
    };

    /**
	* @method getSelectedIndex 
	* Returns selectedIndex.
    * @iatStudioExposed
	* @return {Integer}
	*/
    p.getSelectedIndex = function () {
        return this.settings.selectedIndex;
    };

    /**
	* @method setTabPosition
	* Sets tabPosition
	* @param {brease.enum.TabPosition} tabPosition
	*/
    p.setTabPosition = function (tabPosition) {
        this.settings.tabPosition = tabPosition;
        _setClasses(this);
        _tabBarPosition(this);
    };

    /**
	* @method getTabPosition 
	* Returns tabPosition.
	* @return {brease.enum.TabPosition}
	*/
    p.getTabPosition = function () {
        return this.settings.tabPosition;
    };

    p._tabClickHandler = function (tabId) {
        if (((this.isDisabled === true) && (brease.config.editMode === false))) {
            return;
        }

        var index = this.contents.map(function (content) { return content.id; }).indexOf(tabId);
        if (this.settings.oldIndex === index) {
            return;
        } else {
            this.setSelectedIndex(index);

            this.submitChange();
        }


    };

    p.showHideContents = function (showIndex) {
        if (this.contents.length === 0) { return;}
        for (var i = 0; i < this.contents.length; i += 1) {
            if (i === showIndex) {
                brease.callWidget(this.contents[i].id, 'show');
            } else {
                brease.callWidget(this.contents[i].id, 'hide');
            }
        }
        this.settings.oldIndex = showIndex;
    };

    p.submitChange = function () {
        /**
        * @event change
        * Fired when selected index has changed    
        * @param {Integer} selectedIndex
        * See at {@link brease.events.BreaseEvent#static-property-CHANGE BreaseEvent.CHANGE} for event type  
        * @eventComment
        */
        this.dispatchEvent(new CustomEvent(BreaseEvent.CHANGE, {
            detail: {
                selectedIndex: this.settings.selectedIndex,
            }
        }));
        this.sendValueChange({
            selectedIndex: this.getSelectedIndex(),
        });

        /**
        * @event SelectedIndexChanged
        * @param {Integer} value selected index
        * @iatStudioExposed
        * Fired when index changes.
        */
        var ev = this.createEvent('SelectedIndexChanged', { value: this.getSelectedIndex() });
        ev.dispatch();
    };

    p.widgetAddedHandler = function (e) {
        var tabId = e.detail.widgetId;
        e.stopPropagation();
        if ($('#' + tabId).attr('data-brease-widget').indexOf('TabItem') !== -1) {
            //   _initializeTabItem(this, $('#' + tabId)[0], this.tabBar);
            //Add in Document Order: 
            this.tabBar.remove();
            _findTabs(this);
        }

    };

    p.widgetRemovedHandler = function (e) {

        var tabId = e.detail.widgetId,
            widget = this,
            elTabElem,
            elTabElemId;

        /*$.grep is filtering content array and $.each is going through tabBar children*/

        this.contents = $.grep(this.contents, function (tabElem) {
            elTabElem = $(tabElem);
            elTabElemId = elTabElem.attr('id');
            if ((elTabElemId === tabId) && (elTabElemId.length === tabId.length)) {
                widget.tabBar.children().each(function (index, tabButton) {
                    var elTabButton = $(tabButton),
                        elTabButtonId = elTabButton.attr('id');
                    if (elTabButtonId === tabId + '_breaseTabItemTab') {
                        elTabButton.remove();
                    }
                });
                return true;
            }
        }, true);
    };

    p.dispose = function () {

        this.el.off();

        $.each(this.contents, function (index, tabItem) {
            $(tabItem).off();
        });
        SuperClass.prototype.dispose.call(this);
    };

    function _createTabItemAddControl(widget) {
        var dataAttr = {
            'data-action': 'createWidget',
            'data-action-config': '{"type" : "widgets/brease/TabItem", "parentRefId": "' + widget.el.attr('id') + '" }'
        },
            elControl = $("<div>");

        elControl.addClass("iatd-action").attr(dataAttr);
        elControl.css({
            top: 0, right: 0, position: "absolute", zIndex: 500
        });
        widget.elControlAdd = elControl.appendTo(widget.el);
    }

    function _findTabs(widget) {
        var tabDefs = [];

        widget.tabCount = 0;
        widget.el.find('[data-brease-widget]').each(function () {
            var button = this,
				id = button.id,
				d;

            if ($(button).attr('data-brease-widget').indexOf('TabItem') !== -1) {
                d = $.Deferred();
                tabDefs.push(d);
                if (uiController.getWidgetState(id) >= Enum.WidgetState.INITIALIZED) {
                    d.resolve();
                } else {
                    button.addEventListener(BreaseEvent.WIDGET_INITIALIZED, function (e) {
                        d.resolve();
                    });
                }
            }
        });

        $.when.apply($, tabDefs).done(function () {
            //console.log("tabsFound");
            _addTabBar(widget);
        });


    }

    function _addTabBar(widget) {
        var elTabBar = $('<div class="tabbar" />'),
			tab,
			tabs = [];

        widget.contents = [];

        widget.el.children('.container').children('[data-brease-widget]').each(function () {
            var tabItem = this,
				id = tabItem.id;
            if ($(this).attr('data-brease-widget').indexOf('TabItem') !== -1) {
                _initializeTabItem(widget, tabItem, elTabBar);
            }
        });

        widget.tabBar = elTabBar;
        _tabBarPosition(widget);
        widget.setSelectedIndex(widget.settings.selectedIndex);
        if (brease.config.editMode) {
            widget.elControlAdd = widget.elControlAdd.insertAfter(widget.container);
        }
    }

    function _initializeTabItem(widget, tabItem, tabBar) {
        var tab, tabWidget = brease.callWidget(tabItem.id, 'widget');
        tab = tabWidget.getTabElement();

        if (brease.config.editMode) {
            tab.on(BreaseEvent.EDIT.CLICK, function () {
                widget._tabClickHandler(tabItem.id);
            });
        }

        tabWidget.setParentId(widget.elem.id);
        tabBar.append(tab);
        widget.contents.push(tabItem);
    }

    function _setClasses(widget) {
        var imgClass;

        switch (widget.settings.tabPosition) {
            case Enum.Position.left:
                imgClass = 'tabs-left';
                break;

            case Enum.Position.right:
                imgClass = 'tabs-right';
                break;

            case Enum.Position.top:
                imgClass = 'tabs-top';
                break;

            case Enum.Position.bottom:
                imgClass = 'tabs-bottom';
                break;

        }
        widget.el.removeClass('tabs-left tabs-right tabs-top tabs-bottom');
        widget.el.addClass(imgClass);
    }

    function _tabBarPosition(widget) {
        if (widget.settings.tabPosition === "Top" || widget.settings.tabPosition === "Left" || widget.settings.tabPosition === Enum.Position.left || widget.settings.tabPosition === Enum.Position.top) {
            widget.el.prepend(widget.tabBar);

            if (brease.config.editMode) {
                widget.el.prepend(widget.elControlAdd);
            }

        } else if (widget.settings.tabPosition === "Bottom" || widget.settings.tabPosition === "Right" || widget.settings.tabPosition === Enum.Position.bottom || widget.settings.tabPosition === Enum.Position.right) {
            widget.el.append(widget.tabBar);

            if (brease.config.editMode) {
                widget.el.append(widget.elControlAdd);
            }
        }
    }

    return dragAndDropCapability.decorate(WidgetClass, false);
});