define(['brease/enum/IAT_Enum'], function (EnumObj) {

    'use strict';

    /** 
    * @enum {Integer} brease.controller.objects.ContentStatus
    * @alternateClassName ContentStatus
    */
    /** 
    * @property {Integer} notExistent=-3
    */
    /** 
    * @property {Integer} deactivated=-2
    */
    /** 
    * @property {Integer} deactivatePending=-1
    */
    /** 
    * @property {Integer} initialized=0
    */
    /** 
    * @property {Integer} activatePending=1
    */
    /** 
    * @property {Integer} active=2
    */
    /**
    * @property {Integer} preCached=3
    */
    var ContentStatus = new EnumObj({
        notExistent: -3,
        deactivated: -2,
        deactivatePending: -1,
        initialized: 0,
        activatePending: 1,
        active: 2,
        preCached: 3
    });

    return ContentStatus;
});
