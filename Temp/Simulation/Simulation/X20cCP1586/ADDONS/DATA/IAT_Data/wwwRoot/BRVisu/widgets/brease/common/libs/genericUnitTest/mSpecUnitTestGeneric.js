/*global define, require, jasmine, describe, it, expect, runs, waitsFor, spyOnEvent, m*/
define([
    'widgets/brease/common/libs/genericUnitTest/TestUtils/GenericUnitTestUtils',
    'widgets/brease/common/libs/genericUnitTest/Init/GenericUnitTestInit',
    'widgets/brease/common/libs/genericUnitTest/TestUtils/GenericUnitTestMatchers',
    'widgets/brease/common/libs/Test/Jasmine-moduleTest'
],function (GenericUnitTestUtils,GenericUnitTestInit,GenericUnitTestMatchers) {

    'use strict';

    return {
        suite: function (specParam) {    

            m.describe(specParam.run, 'UnitTest:' ,function(){   
                var mut;
                
                m.describe(specParam.init.run, 'initialization:' ,function(){   

                    m.it(specParam.init.run, 'check initialization of module' ,function(){  
                        GenericUnitTestInit.initModule(specParam.init);
                    });
                });

                m.describe(specParam.functions.run, 'functions:' ,function(){                 
                 
                    beforeEach(function () {
                        mut = GenericUnitTestInit.initModule(specParam.init);
                        this.removeAllSpies();
                    });

                    afterEach(function(){
                        mut = null;
                    });

                    for(var functionName in specParam.functions.functionUnderTest){

                        var  futParam = specParam.functions.functionUnderTest[functionName];

                        (function(functionName,futParam, GenericUnitTestMatchers){

                            beforeEach(function () {
                                this.addMatchers(GenericUnitTestMatchers);
                            });

                            m.cases(futParam.testCases)
                            .caseDescription("$$0")
                            .it(futParam.run, functionName, function (desc, testCase) {                                
                                GenericUnitTestUtils.testSetup(mut, testCase.setUpData);
                                GenericUnitTestUtils.addFunctionsSpies(mut , testCase.functionsToSpyOn);
                                
                                var returnedValue = mut[functionName].apply(mut, testCase.args);
                                expect(returnedValue).toEqual(testCase.return);

                                GenericUnitTestUtils.checkFunctionSpies(mut, testCase.functionsToSpyOn, GenericUnitTestMatchers);
                                GenericUnitTestUtils.testTearDown(mut, testCase.setUpData);
                            });
                        })(functionName,futParam,GenericUnitTestMatchers);
                    }
                });
            });
        }
    };
});
