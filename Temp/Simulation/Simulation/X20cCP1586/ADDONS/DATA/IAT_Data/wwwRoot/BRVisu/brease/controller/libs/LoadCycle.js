define(['brease/events/EventDispatcher', 'brease/events/BreaseEvent', 'brease/core/libs/Deferred', 'brease/controller/ContentManager'], 
    function (EventDispatcher, BreaseEvent, Deferred, contentManager) {

        'use strict';

        // LoadCycle is a helper class to avoid that ContentLoaders are suspended and disposed in one JS "event loop".
        // As long a LoadCycle is running, ContentLoaders are marked for suspension instead of immediate suspension.
        // When a LoadCycle is finished, all marked ContentLoaders are suspended, if they are not disposed during the cycle.
        // This way it's ensured that every ContentLoader is either disposed or suspended.

        var LoadCycle = function (injectedContentManager) {
            this.inProgress = false;
            this.loadStack = [];
            this.contentsToLoad = [];
            this.waitQueue = [];
            this.contentManager = injectedContentManager || contentManager;
            this.contentActivatedListener = _contentActivatedListener.bind(this);
            this.contentDeactivatedListener = _contentDeactivatedListener.bind(this);
        };

        LoadCycle.prototype = new EventDispatcher();

        LoadCycle.prototype.start = function (callback, contentsToLoad, contentsToRemove, callbackInfo) {        
            //console.log('%c' + 'LoadCycle.start:' + JSON.stringify(contentsToLoad) + '/' + JSON.stringify(contentsToRemove) + ',inProgress=' + this.inProgress, 'color:' + ((this.inProgress) ? 'red' : 'green'));

            if (this.inProgress === false) {
                this.inProgress = true;
                this.callback = callback;
                this.callbackInfo = callbackInfo || {};
                this.contentsToRemove = contentsToRemove || [];

                document.body.addEventListener(BreaseEvent.CONTENT_ACTIVATED, this.contentActivatedListener);
                document.body.addEventListener(BreaseEvent.CONTENT_SUSPENDED, this.contentDeactivatedListener);
                document.body.addEventListener(BreaseEvent.CONTENT_DISPOSED, this.contentDeactivatedListener);
            
            } else {
                if (this.callbackInfo.embedded === undefined) {
                    this.callbackInfo.embedded = [];
                }
                this.callbackInfo.embedded.push({ containerId: callbackInfo.containerId, pageId: callbackInfo.pageId });
            }
            var embedCall = callbackInfo && callbackInfo.embedCall === true;
            if (!embedCall) {
                addToArray(this.loadStack, contentsToLoad);
                addToArray(this.contentsToLoad, contentsToLoad);
                
                if (this.loadStack.length === 0 && this.contentsToRemove.length === 0) {
                    _finish.call(this);
                } 
            }
        };

        function addToArray(arr, toAdd) {
            if (Array.isArray(toAdd)) {
                for (var i = 0; i < toAdd.length; i += 1) {
                    if (arr.indexOf(toAdd[i]) === -1) {
                        arr.push(toAdd[i]);
                    }
                }
            }
        }

        LoadCycle.prototype.remove = function (contentId, removeFromContents) {
            //console.debug('LoadCycle.remove:' + contentId + ',removeFromContents:' + removeFromContents);
        
            if (removeFromContents === true) {
                _removeFromStack.call(this, contentId);
            } else {
                if (this.contentManager.isContentActive(contentId)) {
                    _removeFromStack.call(this, contentId);
                } else {
                    _waitForContentActivated.call(this, contentId);
                }
            }
            if (removeFromContents === true && Array.isArray(this.callbackInfo.contentsToLoad)) {
                var index = this.callbackInfo.contentsToLoad.indexOf(contentId);
                if (index !== -1) {
                    this.callbackInfo.contentsToLoad.splice(index, 1);
                }
            }
            if (this.loadStack.length === 0 && this.inProgress === true) {
                _finishCallback.call(this);
            }
        };

        function _removeFromStack(contentId) {
            var index = this.loadStack.indexOf(contentId);
            if (index !== -1) {
                this.loadStack.splice(index, 1);
            }
        }

        function _waitForContentActivated(contentId) {
            this.waitQueue.push(contentId);
        }

        function _contentActivatedListener(e) {
            //console.log(e.type, e.detail.contentId);
            var contentId = e.detail.contentId,
                index = this.waitQueue.indexOf(contentId);
            if (index !== -1) {
                this.waitQueue.splice(index, 1);
                _removeFromStack.call(this, contentId);
                if (this.loadStack.length === 0 && this.inProgress === true) {
                    _finishCallback.call(this);
                }
            }
        }

        function _contentDeactivatedListener(e) {
            //console.log(e.type, e.detail.contentId);
            var contentId = e.detail.contentId,
                index = this.contentsToRemove.indexOf(contentId);
            if (index !== -1) {
                this.contentsToRemove.splice(index, 1);
            }
            // if a content is deactivated before the load cycle is finished: 
            // e.g. close a dialog before its finished
            var index2 = this.waitQueue.indexOf(contentId);
            if (index2 !== -1) {
                this.waitQueue.splice(index, 1);
                _removeFromStack.call(this, contentId);
                if (this.loadStack.length === 0 && this.inProgress === true) {
                    _finishCallback.call(this);
                }
            }
            if (this.loadStack.length === 0 && this.contentsToRemove.length === 0 && this.inProgress === true) {
                _finish.call(this);
            }
        }

        Object.defineProperty(LoadCycle.prototype, 'isEmpty', {
            get: function () {
                return this.length === 0;
            },
            enumerable: true,
            configurable: false
        });

        Object.defineProperty(LoadCycle.prototype, 'length', {
            get: function () {
                return this.loadStack.length;
            },
            enumerable: true,
            configurable: false
        });

        function _finishCallback() {
            //console.log('LoadCycle._finishCallback:' + this.loadStack.length, this.contentsToRemove.length, this.inProgress);
            if (typeof this.callback === 'function') {
                this.callback(this.callbackInfo);
                this.callback = null;
                this.callbackInfo = null;
            }
            if (this.loadStack.length === 0 && this.contentsToRemove.length === 0 && this.inProgress === true) {
                _finish.call(this);
            }
        }

        function _finish() {
            this.inProgress = false;
            this.loadStack = [];
            this.contentsToLoad = [];
            document.body.removeEventListener(BreaseEvent.CONTENT_ACTIVATED, this.contentActivatedListener);
            document.body.removeEventListener(BreaseEvent.CONTENT_SUSPENDED, this.contentDeactivatedListener);
            document.body.removeEventListener(BreaseEvent.CONTENT_DISPOSED, this.contentDeactivatedListener);
            //console.log('LoadCycle.CycleFinished');
            this.dispatchEvent({ type: 'CycleFinished' });
            this.removeEventListener('CycleFinished'); 
        }

        return LoadCycle;

    });
