/*global define,brease,console,CustomEvent,_*/
define(['brease/core/ContainerWidget',
    'brease/decorators/LanguageDependency',
    'brease/enum/Enum',
    'brease/core/Types',
    'brease/events/BreaseEvent',
    'widgets/brease/common/libs/wfUtils/UtilsImage',
    'brease/decorators/DragAndDropCapability'], function (SuperClass, languageDependency, Enum, Types, BreaseEvent, UtilsImage, dragAndDropCapability) {
    /*jshint white:false */
    'use strict';

	/**
	* @class widgets.brease.TabItem
	* #Description
	* widget which represents a tab in the TabControl Widget
	* @breaseNote 
	* @extends brease.core.ContainerWidget
    *
    * @mixins widgets.brease.common.DragDropProperties.libs.DroppablePropertiesEvents
	*
	* @iatMeta category:Category
	* Container
	* @iatMeta description:short
	* Tab plus content in a TabController
	* @iatMeta description:de
	* Konfiguriert einen Tab im TabController
	* @iatMeta description:en
	* Configures one tab in a TabController
	* @iatMeta studio:isContainer
	* true
	*/

    /**
    * @property {WidgetList} [parents=["widgets.brease.TabControl"]]
    * @inheritdoc  
    */
	/**
	* @cfg {ImagePath} image=''
	* @iatStudioExposed
	* @bindable
    * @iatCategory Appearance
	* Path to an optional image.
    * <br>When svg - graphics are used, be sure that in your *.svg-file height and width attributes are specified on the &lt;svg&gt; element.
    * For more detailed information see https://www.w3.org/TR/SVG/struct.html (chapter 5.1.2)
	*/

	/**
	* @cfg {ImagePath} mouseDownImage=''
	* @iatStudioExposed
    * @iatCategory Appearance
	* Path to an optional image for mouseDown.
    * <br>When svg - graphics are used, be sure that in your *.svg-file height and width attributes are specified on the &lt;svg&gt; element.
    * For more detailed information see https://www.w3.org/TR/SVG/struct.html (chapter 5.1.2)
	*/

	/**
	* @cfg {brease.enum.ImageAlign} imageAlign='left'
	* @iatStudioExposed
    * @iatCategory Appearance
	* Position of image relative to text. 
	*/

	/**
	* @cfg {String} text=''
    * @localizable
	* @iatStudioExposed
	* @bindable
    * @iatCategory Appearance
	* Text which is displayed in the button
	*/

	/**
	* @cfg {Boolean} ellipsis=false
	* @iatStudioExposed
    * @iatCategory Behavior
	* If true, overflow of text is symbolized with an ellipsis. This option has no effect, if wordWrap = true.
	*/

	/**
	* @cfg {Boolean} wordWrap=false
	* @iatStudioExposed
    * @iatCategory Behavior
	* If true, text will wrap when necessary.
	*/

	/**
	* @cfg {Boolean} multiLine=false
	* @iatStudioExposed
    * @iatCategory Behavior
	* If true, more than one line is possible. Text will wrap when necessary (wordWrap=true) or at line breaks (\n).
	* If false, text will never wrap to the next line. The text continues on the same line.
	*/
	var defaultSettings = {
	    imageAlign: Enum.ImageAlign.left,
	    textAlign: Enum.TextAlign.center,
	    ellipsis: false,
	    wordWrap: false,
	    multiLine: false,
	    height: 30,
	    width: 100
	},

	WidgetClass = SuperClass.extend(function TabItem() {
	    SuperClass.apply(this, arguments);
	}, defaultSettings),

	p = WidgetClass.prototype;

    p.init = function () {

        if (this.settings.omitClass !== true) {
            this.el.addClass('breaseTabItemContainer');
        }
        this._createTab();
        this._eventHandling();

        if (this.settings.visible === false && brease.config.editMode !== true) {
            _setVisible.call(this, this.settings.visible);
        }

        SuperClass.prototype.init.call(this);

        this.setStyle(this.settings.style);

        if (brease.config.editMode === true) {
            _initEditor(this);
        }

    };

    p.setStyle = function (style) {
        this.tab.removeClass(this.settings.stylePrefix + '_style_' + this.settings.style);
        this.settings.style = style;
        this.tab.addClass(this.settings.stylePrefix + '_style_' + this.settings.style);
    };

    p._setWidth = function (w) {
        this.settings.width = w;
        this.tab.css('width', this.settings.width);
    };

    p._setHeight = function (h) {
        this.settings.height = h;
        this.tab.css('height', this.settings.height);
    };


    p.getTabElement = function () {
        return this.tab;

    };

    /**
    * @method setLeft
    * Sets left
    * @param {Integer} left
    */
    p.setLeft = function (left) {
        this.settings.left = left;
        this.tab.css("left", this.settings.left + 'px');
    };

    p.show = function () {
        this.tab.addClass("active");
        _setVisible.call(this, undefined);
        this.containerVisibility = true;
        this.updateChildrenVisibility();
        _imageHandling(this);
        this.elem.dispatchEvent(new CustomEvent(BreaseEvent.VISIBILITY_CHANGED, { bubbles: false, detail: { visible: !this.isHidden } }));
    };

    p.hide = function () {
        this.tab.removeClass("active");
        _setVisible.call(this, undefined);
        this.containerVisibility = false;
        this.updateChildrenVisibility();
        _imageHandling(this);
    };

    p._eventHandling = function () {
        this.tab.on(BreaseEvent.CLICK, this._bind('_clickHandler'));
        this.tab.on(BreaseEvent.MOUSE_DOWN, this._bind('_downHandler'));
        this.tab.on(BreaseEvent.MOUSE_UP, this._bind('_upHandler'));

    };

    p._downHandler = function (e) {


    };

    p._upHandler = function (e) {

    };

    p._createTab = function () {
        var tabId = this.elem.id + '_breaseTabItemTab';
        this.tab = $("<div id='" + tabId + "'class='tabItem breaseTabItem' data-brease-container=" + this.elem.id + " />").width(this.settings.width).height(this.settings.height);
        
        _appendImgEl(this);
        _multiLineWordWrapEllipsis(this);

        if (this.settings.text !== undefined) {
            if (brease.language.isKey(this.settings.text) === false) {
                this.setText(this.settings.text);
            } else {
                this.setTextKey(brease.language.parseKey(this.settings.text));
            }
        }
        if (this.settings.image !== undefined) {
            this.setImage(this.settings.image);
        }

        if (this.settings.left !== undefined) {
            this.tab.css("left", this.settings.left + 'px');
        }

    };

    p.suspend = function () {
        this.tab.off(BreaseEvent.CLICK, this._bind('_clickHandler'));
        SuperClass.prototype.suspend.apply(this, arguments);
    };

    p.wake = function () {
        this.tab.on(BreaseEvent.CLICK, this._bind('_clickHandler'));
        SuperClass.prototype.wake.apply(this, arguments);
    };

    p.setParentId = function (parentId) {
        this.settings.parentId = parentId;
    };

    p._clickHandler = function (e) {
        if (this.settings.parentId !== undefined && e.currentTarget.id === this.tab[0].id) {
            brease.callWidget(this.settings.parentId, '_tabClickHandler', this.elem.id);
        }
        SuperClass.prototype._clickHandler.call(this, e, { origin: this.elem.id });
    };

    p.updateVisibility = function (initial) {
        SuperClass.prototype.updateVisibility.apply(this, arguments);
        _setVisible.call(this, !this.isHidden);
    };


    p.langChangeHandler = function (e) {
        if (this.settings.textkey && (e === undefined || e.detail === undefined || e.detail.textkey === undefined || e.detail.textkey === this.settings.textkey)) {
            this.setText(brease.language.getTextByKey(this.settings.textkey), true);
        }
    };

    /**
	* @method setText
	* @iatStudioExposed
	* Sets the visible text. This method can remove an optional textkey.
	* @param {String} text
	* @param {Boolean} keepKey Set true, if textkey should not be removed.
	*/
    p.setText = function (text, keepKey) {
        this.settings.text = text;

        if (brease.language.isKey(this.settings.text) === false) {
            if (keepKey !== true) {
                this.removeTextKey();
            }
            if (this.textEl === undefined) {
                _appendTextEl(this);
            }
            this.textEl.text(text);

            _setClasses(this);
        } else {
            this.setTextKey(brease.language.parseKey(this.settings.text));
        }



    };

    /**
	* @method getText
	* Returns the visible text.
	* @return {String} text
	*/
    p.getText = function () {
        return this.settings.text;
    };

    /**
	* @method setTextKey
	* set the textkey
	* @param {String} key The new textkey
	*/
    p.setTextKey = function (key) {
        if (key !== undefined) {
            this.settings.textkey = key;
            this.setLangDependency(true);
            this.langChangeHandler();
        }
    };

    /**
   * @method removeTextKey
   * remove the textkey
   */
    p.removeTextKey = function () {
        this.settings.textkey = null;
        this.setLangDependency(false);
    };

    /**
   * @method removeText
   * @iatStudioExposed
   * Remove text.
   */
    p.removeText = function () {
        this.setText('');
        _setClasses(this);
    };

    /**
	* @method getTextKey
	* get the textkey
	*/
    p.getTextKey = function () {
        return this.settings.textkey;
    };

    /**
	* @method setImage
	* @iatStudioExposed
	* Sets an image.
	* @param {ImagePath} image
	*/
    p.setImage = function (image, omitSettings) {
        this.settings.image = image;
        _imageHandling(this);
        _setClasses(this);
    };

    /**
	* @method getImage
	* Returns the path of the image.
	* @return {ImagePath} text
	*/
    p.getImage = function () {
        return this.settings.image;
    };

    /**
	* @method removeImage
	* @iatStudioExposed
	* Remove an image.
	*/
    p.removeImage = function () {
        _hideImages(this);
        this.settings.image = undefined;
        _setClasses(this);
    };



    /**
	* @method setMouseDownImage
	* Sets mouseDownImage
	* @param {ImagePath} mouseDownImage
	*/
    p.setMouseDownImage = function (mouseDownImage) {
        this.settings.mouseDownImage = mouseDownImage;
        _imageHandling(this);
        _setClasses(this);
    };

    /**
	* @method getMouseDownImage 
	* Returns mouseDownImage.
	* @return {ImagePath}
	*/
    p.getMouseDownImage = function () {
        return this.settings.mouseDownImage;
    };

    /**
	* @method setImageAlign
	* Sets imageAlign
	* @param {brease.enum.ImageAlign} imageAlign
	*/
    p.setImageAlign = function (imageAlign) {
        this.settings.imageAlign = imageAlign;

        if (imageAlign === Enum.ImageAlign.left || imageAlign === Enum.ImageAlign.top) {
            this.tab.append(this.textEl);
        } else if (imageAlign === Enum.ImageAlign.right || imageAlign === Enum.ImageAlign.bottom) {
            this.tab.prepend(this.textEl);
        }

        _setClasses(this);
    };

    /**
	* @method getImageAlign 
	* Returns imageAlign.
	* @return {brease.enum.ImageAlign}
	*/
    p.getImageAlign = function () {
        return this.settings.imageAlign;
    };


    /**
	* @method setEllipsis
	* Sets ellipsis
	* @param {Boolean} ellipsis
	*/
    p.setEllipsis = function (ellipsis) {
        this.settings.ellipsis = ellipsis;
        _multiLineWordWrapEllipsis(this);
    };

    /**
	* @method getEllipsis 
	* Returns ellipsis.
	* @return {Boolean}
	*/
    p.getEllipsis = function () {
        return this.settings.ellipsis;
    };


    /**
	* @method setWordWrap
	* Sets wordWrap
	* @param {Boolean} wordWrap
	*/
    p.setWordWrap = function (wordWrap) {
        this.settings.wordWrap = wordWrap;
        _multiLineWordWrapEllipsis(this);
    };

    /**
	* @method getWordWrap 
	* Returns wordWrap.
	* @return {Boolean}
	*/
    p.getWordWrap = function () {
        return this.settings.wordWrap;
    };


    /**
	* @method setMultiLine
	* Sets multiLine
	* @param {Boolean} multiLine
	*/
    p.setMultiLine = function (multiLine) {
        this.settings.multiLine = multiLine;
        _multiLineWordWrapEllipsis(this);
    };

    /**
	* @method getMultiLine 
	* Returns multiLine.
	* @return {Boolean}
	*/
    p.getMultiLine = function () {
        return this.settings.multiLine;
    };

    p.dispose = function () {
        this.tab.off();
        $(document).off(BreaseEvent.MOUSE_UP, this._bind('_upHandler'));
        SuperClass.prototype.dispose.call(this);
    };



    function _appendTextEl(widget) {
        widget.textEl = $('<span></span>');
        if (widget.imgEl && (widget.settings.imageAlign === Enum.ImageAlign.right || widget.settings.ImageAlign === Enum.ImageAlign.bottom)) {
            widget.tab.prepend(widget.textEl);
        } else {
            widget.tab.append(widget.textEl);
        }
    }

    function _appendImgEl(widget) {
        widget.imgEl = $('<img/>');
        widget.svgEl = $('<svg/>');
        if (widget.textEl && (widget.settings.imageAlign === Enum.ImageAlign.left || widget.settings.imageAlign === Enum.ImageAlign.top)) {
            widget.tab.prepend(widget.imgEl);
            widget.tab.prepend(widget.svgEl);
        } else {
            widget.tab.append(widget.imgEl);
            widget.tab.append(widget.svgEl);
        }

        widget.imgEl.hide();
        widget.svgEl.hide();

    }

    function _setVisible(value) {
        if (this.isHidden === false) {
            this.tab.removeClass('remove');
            if ($(this.tab).hasClass('active')) {
                this.el.removeClass('remove');
            } else {
                this.el.addClass('remove');
            }
        } else {
            this.tab.addClass('remove');
            this.el.addClass('remove');
        }

    }

    function _multiLineWordWrapEllipsis(widget) {

        widget.settings.multiLine = Types.parseValue(widget.settings.multiLine, 'Boolean');
        widget.settings.wordWrap = Types.parseValue(widget.settings.wordWrap, 'Boolean');
        widget.settings.ellipsis = Types.parseValue(widget.settings.ellipsis, 'Boolean');

        if (widget.settings.multiLine === true) {
            if (widget.settings.wordWrap === true) {
                widget.tab.addClass('wordWrap');
                widget.tab.removeClass('multiLine');
            } else {
                widget.tab.addClass('multiLine');
                widget.tab.removeClass('wordWrap');
            }
        } else {
            widget.tab.removeClass('multiLine');
            widget.tab.removeClass('wordWrap');
        }



        if (widget.settings.ellipsis === true) {
            widget.tab.addClass('ellipsis');
        } else {
            widget.tab.removeClass('ellipsis');
        }
    }

    function _setClasses(widget) {
        var imgClass;
        if (widget.imgEl !== undefined && widget.textEl !== undefined && widget.settings.text !== '') {

            widget.tab.removeClass('image-left image-right image-top image-bottom');

            switch (widget.settings.imageAlign) {
                case Enum.ImageAlign.left:
                    imgClass = 'image-left';
                    break;

                case Enum.ImageAlign.right:
                    imgClass = 'image-right';
                    break;

                case Enum.ImageAlign.top:
                    imgClass = 'image-top';
                    break;

                case Enum.ImageAlign.bottom:
                    imgClass = 'image-bottom';
                    break;

            }

            widget.tab.addClass(imgClass);
        } else {
            widget.tab.removeClass('image-left image-right image-top image-bottom');
        }
    }

    function _imageHandling(widget) {

        if ($(widget.tab).hasClass('active')) {
            if (widget.settings.mouseDownImage !== undefined && widget.settings.mouseDownImage !== '') {
                _imageSet(widget, widget.settings.mouseDownImage);
            } else if ((widget.settings.mouseDownImage === undefined || widget.settings.mouseDownImage === '') && widget.settings.image !== undefined && widget.settings.image !== '') {
                _imageSet(widget, widget.settings.image);
            } else {
                _hideImages(widget);
            }
        } else {
            if (widget.settings.image !== undefined && widget.settings.image !== '') {
                _imageSet(widget, widget.settings.image);
            } else {
                _hideImages(widget);
            }
        }
    }

    function _imageSet(widget, image) {
        
        if (UtilsImage.isStylable(image)) {
            widget.imgEl.hide();
            UtilsImage.getInlineSvg(image).then(function (svgElement) {
                widget.svgEl.replaceWith(svgElement);
                widget.svgEl = svgElement;
                widget.svgEl.show();
            });
        } else {
            widget.imgEl.show();
            widget.svgEl.hide();
            widget.imgEl.attr('src', image);
        }
    }

    function _hideImages(widget) {
        widget.svgEl.hide();
        widget.imgEl.hide();
    }

    function _initEditor(widget) {

        widget.isRotatable = function () { return false; };
        widget.isResizable = function () { return false; };

        require(['widgets/brease/TabItem/libs/EditorHandles'], function (EditorHandles) {
            var editorHandles = new EditorHandles(widget);
            widget.getHandles = function () {
                return editorHandles.getHandles();
            };
            widget.designer.getSelectionDecoratables = function () {
                return widget.el;
            };
        });
    }

    return dragAndDropCapability.decorate(languageDependency.decorate(WidgetClass, false), false);

});